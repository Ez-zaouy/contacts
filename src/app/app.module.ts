import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HttpModule} from '@angular/http';
import {ContactsServices} from '../services/contacts.services'
import {FormsModule} from '@angular/Forms'
import { AppComponent } from './app.component';
import { ContactsComponent } from './contacts/contacts.component';
import { AboutComponent } from './about/about.component';
import { TestComponent } from './test/test.component';
import { NewContactComponent } from './new-contact/new-contact.component';
import { WhateverComponent } from './whatever/whatever.component';
import { NouveauContactComponent } from './nouveau-contact/nouveau-contact.component';
import { EditContactComponent } from './edit-contact/edit-contact.component';
import { DeleteContactComponent } from './delete-contact/delete-contact.component';


 const appRoutes : Routes = [
 	{path :'about', component :AboutComponent},
 	{path :'contacts', component :ContactsComponent},
   {path :'new-contact', component :NouveauContactComponent},
   {path: 'edit-contact/:id', component: EditContactComponent},
   {path:'delete-contact/:id',component:DeleteContactComponent}
 	 {path:'',redirectTo:'/about',pathMatch: 'full'}

     ];
@NgModule({
  declarations: [
    AppComponent,
    ContactsComponent,
    AboutComponent,
    TestComponent,
    NewContactComponent,
    WhateverComponent,
    NouveauContactComponent,
    EditContactComponent,
    DeleteContactComponent
  ],
  imports: [
    BrowserModule, RouterModule.forRoot(appRoutes),HttpModule,FormsModule
  ],
  providers: [ContactsServices],
  bootstrap: [AppComponent]
})
export class AppModule { }
