import { Component, OnInit } from '@angular/core';
import {Http} from '@angular/http';
import { map } from 'rxjs/operators';
import {ContactsServices} from '../../services/contacts.services';
import { Router } from '@angular/router';
@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {


  pageContacts:any;
  pages:any;


  motCle: string= "a";
  Currentpage: number=0;
  size: number=5

  constructor(public http:Http, public contactservice:ContactsServices,public router:Router) { }

  ngOnInit() {

  }

doSearch(){
  this.contactservice.getContacts(this.motCle,this.Currentpage,this.size).subscribe(data=>{
      this.pageContacts=data;
      this.pages = new Array(data.totalPages);
    },
    err=>{
      console.log(err);
    })
  }


chercher(){
   this.doSearch();
}
 gotoPage(i:number){
   this.Currentpage=i;
   this.doSearch();
 }
onEdit(id:number){
 this.router.navigate(["edit-contact",id]);

}
onDelete(id:number){
  this.contactservice.deleteContact(id).subscribe(data=>{


  }),
  err=>{

  }

 }
 }


