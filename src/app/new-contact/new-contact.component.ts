import { Component, OnInit } from '@angular/core';
import { Contact } from '../../model/model.contact';
import {ContactsServices} from '../../services/contacts.services';
import {Http} from '@angular/http';

@Component({
  selector: 'app-new-contact',
  templateUrl: './new-contact.component.html',
  styleUrls: ['./new-contact.component.css']
})
export class NewContactComponent implements OnInit {

  mode:number=1;
  contact : Contact = new Contact() ;

  constructor(public http:Http, public contactservice:ContactsServices) { }

  ngOnInit() {


  }
    saveContact() {
      console.log(this.contact);
      this.contactservice.addContact(this.contact).subscribe(data=>{
        console.log(data);
        this.contact=data;
        this.mode=2;
      },
      err=>{
        console.log(err);
      })
      ;

  }

}
