import { Component, OnInit } from '@angular/core';
import { Contact } from 'src/model/model.contact';
import { ContactsServices } from 'src/services/contacts.services';
import { ActionSequence } from 'protractor';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-contact',
  templateUrl: './edit-contact.component.html',
  styleUrls: ['./edit-contact.component.css']
})
export class EditContactComponent implements OnInit {

  contact :Contact = new Contact();
  mode:number=1;
  idContact:number;

  constructor(public contactservice: ContactsServices,public activatedroute: ActivatedRoute,public router:Router) {
    //recupérer l'id du contact mn la route
    this.idContact = activatedroute.snapshot.params['id'];
  }

  ngOnInit() {
    this.contactservice.getContact(this.idContact).subscribe(
      data=>{
        this.contact=data;
      },
      err=>{
        console.log(err);
      }
    )

  }


  editContact(){
    console.log(this.contact);
    this.contactservice.updateContact(this.contact).subscribe(data=>{

      alert("mise à jour effectuer");
      this.router.navigate(["contacts"]);
      //this.mode=2;
    },
    err=>{
      console.log(err);
    })
    ;

}

}

