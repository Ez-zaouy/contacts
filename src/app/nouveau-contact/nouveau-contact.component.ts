import { Component, OnInit } from '@angular/core';
import { ContactsServices } from 'src/services/contacts.services';
import { Parser } from '@angular/compiler/src/ml_parser/parser';

@Component({
  selector: 'app-nouveau-contact',
  templateUrl: './nouveau-contact.component.html',
  styleUrls: ['./nouveau-contact.component.css']
})
export class NouveauContactComponent implements OnInit {

  constructor(public contactservice:ContactsServices) { }

  ngOnInit() {
  }
onSaveContact(dataForm){
  this.contactservice.addContact(dataForm).subscribe(data=>{
    console.log(dataForm);
  },
  err=>{
    console.log(JSON.parse(err._body).message);
  })

}
}
